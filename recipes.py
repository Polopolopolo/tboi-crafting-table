from bs4 import BeautifulSoup
import bottle
import requests
import yaml


URLS = {
    "passives1": "https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Passive_(1-399)",
    "passives2": "https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Passive_(400-727)",
    "actives": "https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Active"
}




def get_items(url):
    html = requests.get(url).text
    soup = BeautifulSoup(html, features="html.parser")
    items = []
    for tr in soup.find_all("tr"):
        try:
            item = {}
            tds = tr.find_all("td", recursive=False)
            if len(tds) > 2 and len(tds[0].find_all("a")) > 1:
                item["title"] = tds[0].find_all("a")[1]["title"]
                if tds[0].find("img")["alt"].startswith("Added in"):
                    item["img"] = tds[0].find_all("img")[1]["src"]
                else:
                    item["img"] = tds[0].find("img")["src"]
                item["id"] = int(tds[1].text)
                item["recipes"] = []
                for recipes_td in tds[2:]:
                    recipe = []
                    for a in recipes_td.find_all("a", attrs={"class": "image"}):
                        recipe.append({"icon": a["href"], "title": a["title"]})
                    item["recipes"].append(recipe)
                items.append(item)
        except Exception as err:
            print (err, err.args)
    return items


def build_recipes_file():
    items = []
    for (k, v) in URLS.items():
        items += get_items(v)
    with open("recipes.yaml", "w") as f:
        yaml.dump(items, f, indent=2)


def build_item_mosaique(items):
    s = """<div class="mosaique">"""
    for item in items:
        s += f"""<img alt="{item['title']}" src="{item['img']}">"""
    return s + "</div>"


def build_recipe_panel():
    return """<div class="recipe-panel"></div>"""


def get_ingredients(items):
    ingredients = set()
    for item in items:
        for recipe in item["recipes"]:
            for ingredient in recipe:
                ingredients.add(tuple(ingredient.values()))
    return list(ingredients)


@bottle.route("/reverse")
@bottle.view("index2.html")
def reverse():
    with open("ingredients.yaml") as f:
        ingredients = yaml.load(f)
    return {"ingredients": ingredients}


@bottle.route("/reverse/<a:int>")
def reverse_arg(a):
    return "bli " * a


def possible_items(items, ingredients):
    possible_items = set()
    for item in items:
        for recipe in item["recipes"]:
            flag = True
            for (title, nb) in ingredients:
                if len([x for x in recipe if x["title"] == title]) < nb:
                    flag = False
            if flag:
                possible_items.add((item["title"], item["id"]))
    return list(possible_items)


if __name__ == "__main__":
    # build_recipes_file()

    # GET ITEMS
    # print ("Parsing recipes...", end="", flush=True)
    # with open("recipes.yaml") as f:
    #     items = yaml.load(f, Loader=yaml.BaseLoader)
    # print ("Done", flush=True)

    # WRITE HTML 1
    # mosaique = build_item_mosaique(items)
    # recipe_panel = build_recipe_panel()

    # html = """<html><head><link rel="stylesheet" href="style.css"></head><body>""" + mosaique + recipe_panel + """</body></html>"""
    # with open("index.html", "w") as f:
    #     f.write(html)

    # WRITE INGREDIENTS
    # with open("ingredients.yaml", "w") as f:
    #     yaml.dump(get_ingredients(items), f)
    pass
