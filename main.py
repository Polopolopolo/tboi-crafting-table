import PyQt5
from PyQt5 import QtWidgets, QtGui, QtCore, uic
import requests
from requests.sessions import extract_cookies_to_jar, to_native_string
import yaml, json

from recipes import possible_items, get_items


class IngredientIcon(QtWidgets.QLabel):
    def __init__(self, url, title):
        super().__init__()
        self.title = title
        self.url = url
        self.setPixmap(QtGui.QPixmap(url))

    def mouseReleaseEvent(self, ev: QtGui.QMouseEvent) -> None:
        table: QtWidgets.QWidget = self.window().craft_table
        nb_empty = len([w for w in table.children()[1:] if w.is_empty])
        if nb_empty == 0:
            for i in range(1, 8):
                table.children()[i].fill(table.children()[i+1].url, self.title)
            table.children()[8].fill(self.url, self.title)
        else:
            for i in range(9 - nb_empty, 1, -1):
                table.children()[i].fill(table.children()[i-1].url, self.title)
            table.children()[1].fill(self.url, self.title)


class RecipeTable(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setLayout(QtWidgets.QGridLayout())
        self.ingredients = []

    def fill(self, ingredients):
        self.ingredients = ingredients
        for (i, ing) in enumerate(ingredients):
            ing_id = int(ing["title"][-4:].replace("(", "").replace(")", " ").replace(" ", ""))
            icon = QtWidgets.QLabel()
            icon.setPixmap(QtGui.QPixmap(f"pictures/ingredient_icon{ing_id}.png"))
            self.layout().addWidget(icon, i // 4, i % 4)


class CraftingTableItem(QtWidgets.QLabel):
    def __init__(self):
        super().__init__()
        self.is_empty = True
        self.empty()

    def fill(self, pixmap_url, title):
        self.url = pixmap_url
        self.title = title
        self.setPixmap(QtGui.QPixmap(pixmap_url))
        self.is_empty = False
        self.window().items_widget.update_items()

    def empty(self):
        self.url = "pictures/empty_craft_item.png"
        self.title = ""
        self.setPixmap(QtGui.QPixmap(self.url))
        self.is_empty = True
        if self.window() is not self:
            self.window().items_widget.update_items()

    def mouseReleaseEvent(self, ev: QtGui.QMouseEvent) -> None:
        if not self.is_empty:
            table: QtWidgets.QWidget = self.window().craft_table
            nb_empty = len([w for w in table.children()[1:] if w.is_empty])
            self_index = table.children().index(self)
            for i in range(self_index, 8 - nb_empty):
                table.children()[i].fill(table.children()[i+1].url, table.children()[i+1].title)
            table.children()[8 - nb_empty].empty()


class Item(QtWidgets.QLabel):
    def __init__(self, i, j):
        self.i = i
        self.j = j
        self.url = ""
        self.item_id = -1
        self.title = ""
        super().__init__()

    def fill(self, url, title, item_id):
        self.title = title
        self.url = url
        self.item_id = item_id
        self.setPixmap(QtGui.QPixmap(f"pictures/item_icon{item_id}.png"))

    def empty(self):
        self.url = ""
        self.item_id = -1
        self.clear()

    def mouseReleaseEvent(self, ev):
        self.window().fill_right_panel(self.item_id)

    def setHighlight(self, b: bool):
        if b:
            self.setStyleSheet("*{border: 2px solid black}")
        else:
            self.setStyleSheet("")

class Items(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setLayout(QtWidgets.QGridLayout())
        self.width = 12
        self.nb_items = 727
        # fill the grid
        for i in range(self.nb_items):
            w = Item(i // self.width, i % self.width)
            self.layout().addWidget(w, i // self.width, i % self.width)
            w.setAlignment(QtCore.Qt.AlignCenter)

    def update_items(self):
        for i in range(self.nb_items):
            self.children()[i+1].empty()

        ingredients = self.window().get_ingredients_from_table()
        items = possible_items(self.window().items, ingredients)
        items.sort(key=lambda item: item[1])
        for (i, item) in enumerate(items):
            self.children()[i+1].fill(f"pictures/item_icon{item[1]}", item[0], item[1])

    def search_items(self, text):
        if text:
            for i in range(self.nb_items):
                if text.strip().lower() in self.children()[i+1].title.strip().lower():
                    self.children()[i+1].setHighlight(True)
                else:
                    self.children()[i+1].setHighlight(False)
        else:
            for i in range(self.nb_items):
                self.children()[i+1].setHighlight(False)


class Searchbar(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setLayout(QtWidgets.QHBoxLayout())
        self.search_bar = QtWidgets.QLineEdit()
        self.search_bar.setPlaceholderText("Search for an item")
        self.search_bar.textChanged.connect(self.searchBarTextChanged)
        self.layout().addWidget(self.search_bar, 1)
        self.layout().addStretch(1)

    def searchBarTextChanged(self, text):
        self.window().items_widget.search_items(text)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("main.ui", self)
        with open("recipes.json") as f:
            self.items = json.load(f)

        # left panel

        ## item zone
        self.left_panel.layout().setStretchFactor(self.craft_zone, 1)
        self.items_widget = Items()
        self.items_scroll = QtWidgets.QScrollArea()
        self.items_scroll.setWidgetResizable(True)
        self.items_scroll.setWidget(self.items_widget)
        self.left_panel.layout().addWidget(self.items_scroll, 3)

        ## ingredients list
        self.ingredients_list = self.get_ingredients()
        for i, ing in enumerate(self.ingredients_list):
            url, title, ing_id = ing
            self.ingredients.layout().addWidget(IngredientIcon(f"pictures/ingredient_icon{ing_id}.png", title), i // 8, i % 8)

        ## craft table
        for i in range(8):
            self.craft_table.layout().addWidget(CraftingTableItem(), i // 4, i % 4)

        ## searchbar
        self.left_panel.layout().addWidget(Searchbar(), 0)

        # right panel
        right_scroll = QtWidgets.QScrollArea()
        right_scroll.setWidgetResizable(True)
        self.right_panel.layout().addWidget(right_scroll, 10)
        self.recipes_container = QtWidgets.QWidget()
        self.recipes_container.setLayout(QtWidgets.QVBoxLayout())
        right_scroll.setWidget(self.recipes_container)


    def get_ingredients(self):
        with open("ingredients.json") as f:
            ingredients = json.load(f)
        return ingredients

    def get_ingredients_from_table(self):
        ingredients = [w.title for w in self.craft_table.children()[1:] if not w.is_empty]
        return set((ing, ingredients.count(ing)) for ing in ingredients)

    def fill_right_panel(self, item_id):
        item = [item for item in self.items if item["id"] == item_id][0]
        self.current_item_icon.setPixmap(QtGui.QPixmap(f"pictures/item_icon{item_id}"))
        self.current_item_title.setText(item["title"])
        for i in range(self.recipes_container.layout().count()):
            w = self.recipes_container.layout().takeAt(0).widget()
            self.recipes_container.layout().removeWidget(w)
            w.setParent(None)
            del w
        for recipe in item["recipes"]:
            recipe_table = RecipeTable()
            recipe_table.fill(recipe)
            self.recipes_container.layout().addWidget(recipe_table)



if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    main_window = MainWindow()
    main_window.show()
    app.exec_()
